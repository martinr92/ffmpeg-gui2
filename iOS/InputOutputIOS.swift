//
//  InputOutputIOS.swift
//  FFmpeg-GUI2 (iOS)
//
//  Created by Martin Riedl on 17.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import UIKit
import UniformTypeIdentifiers

class InputOutput: NSObject, UIDocumentPickerDelegate {

    var checkContinuation: CheckedContinuation<Void, Never>?
    var urls: [URL]?

    func openFiles(source: InputOutputSource) async -> [URL]? {
        switch source {
        case .filesystem:
            return await openFilesystem()
        case .photoLibrary:
            return openPhotoLibrary()
        }
    }

    func outputFile() -> URL? {
        // TODO: find a way to store the output
        return nil
    }

    private func openFilesystem() async -> [URL]? {
        // show document picker
        let pickerController = UIDocumentPickerViewController(forOpeningContentTypes: [UTType.data])
        pickerController.delegate = self
        pickerController.allowsMultipleSelection = true
        UIApplication.shared.windows.first?.rootViewController?.present(pickerController, animated: true) // TODO: relly first window?
        await withCheckedContinuation { (checkContinuation: CheckedContinuation<Void, Never>) in
            self.checkContinuation = checkContinuation
        }

        return urls
    }

    private func openPhotoLibrary() -> [URL]? {
        // TODO: implement photo picker
        return nil
    }

    // MARK: - UIDocumentPickerDelegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        self.urls = urls
        checkContinuation?.resume()
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        checkContinuation?.resume()
    }
}

extension InputOutputSource {
    var isAvailable: Bool {
        switch self {
        case .filesystem:
            return true
        case .photoLibrary:
            return true
        }
    }
}
