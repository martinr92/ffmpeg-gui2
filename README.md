#  FFmpeg GUI 2
[![pipeline status](https://gitlab.com/martinr92/ffmpeg-gui2/badges/main/pipeline.svg)](https://gitlab.com/martinr92/ffmpeg-gui2/-/commits/main)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=security_rating)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=bugs)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=code_smells)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=martinr92_ffmpeg-gui2&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=martinr92_ffmpeg-gui2)

GUI application for macOS, iOS and iPadOS.

## How to use
TODO: link to the app store (mac and mobile)

## How to contribute
Please contribute changes / features / fixes only on the `main` branch. Only the lastest version of iOS, iPadOS and macOS is supported. For more details check [CONTRIBUTING.md](CONTRIBUTING.md).
