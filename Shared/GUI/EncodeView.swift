//
//  EncodeView.swift
//  EncodeView
//
//  Created by Martin Riedl on 25.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct EncodeView: View {
    @ObservedObject var command: Command

    var body: some View {
        SectionView(step: 4, title: "Encoding") {
            VStack(alignment: .leading) {
                Button("Start Encoding") {
                    command.startExecution()
                }.disabled(command.isEncodingRunning)
                if command.isEncodingRunning {
                    Text("Encoding is running")
                }
                if let encodingCompletedSuccessfully = command.encodingCompletedSuccessfully {
                    if encodingCompletedSuccessfully {
                        Text("Encoding completed successfully")
                    } else {
                        Text("Encoding failed")
                    }
                }
                if let encodingLog = command.encodingLog {
                    LogButton(text: encodingLog)
                }
            }
        }
    }
}

private struct LogButton: View {
    var text: String
    @State private var showingSheet = false

    var body: some View {
        Button {
            showingSheet = true
        } label: {
            Text("Show Log")
        }.sheet(isPresented: $showingSheet) {
            LogView(text: text, showingSheet: $showingSheet)
        }
    }
}

private struct LogView: View {
    var text: String
    @Binding var showingSheet: Bool

    var body: some View {
        VStack {
            ScrollView {
                Text(text)
            }
            Button {
                showingSheet = false
            } label: {
                Text("OK")
            }
            .padding()
        }
    }
}

struct EncodeView_Previews: PreviewProvider {
    @State static var command = Command.testData

    static var previews: some View {
        EncodeView(command: command)
    }
}
