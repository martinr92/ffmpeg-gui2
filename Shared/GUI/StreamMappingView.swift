//
//  StreamMappingView.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 10.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct StreamMappingView: View {
    @ObservedObject var command: Command

    var body: some View {
        SectionView(step: 2, title: "Stream Mapping") {
            VStack(alignment: .leading) {
                ForEach(command.outputStreams.indices, id: \.self) { index in
                    StreamView(command: command, streamIndex: index, outputStream: $command.outputStreams[index])
                }
                Button("Add Output Stream") {
                    command.addOutputStream()
                }
            }
        }
    }
}

private struct StreamView: View {
    @ObservedObject var command: Command
    var streamIndex: Int
    @Binding var outputStream: OutputStream

    var body: some View {
        GroupBox {
            VStack {
                HStack {
                    Text("Output Stream \(streamIndex + 1)")
                    Spacer()
                    Button {
                        command.removeOutputStream(index: streamIndex)
                    } label: {
                        Image(systemName: "trash")
                    }
                }
                HStack {
                    SourcePickerView(command: command, selectedSource: $outputStream.inputStream)
                    CodecSettingsButton(outputStream: $outputStream)
                }
            }
        }
    }
}

private struct SourcePickerView: View {
    var command: Command
    @Binding var selectedSource: InputStream?

    var body: some View {
        Picker("Source", selection: $selectedSource) {
            Text("<choose>").tag(nil as InputStream?)
            ForEach(command.inputFiles.indices, id: \.self) { inputFileIndex in
                let inputFile = command.inputFiles[inputFileIndex]
                ForEach(inputFile.streams) { stream in
                    SourcePickerLabelView(inputFileIndex: inputFileIndex, inputFile: inputFile, stream: stream)
                }
            }
        }
    }
}

private struct SourcePickerLabelView: View {
    var inputFileIndex: Int
    var inputFile: InputFile
    var stream: InputStream

    var body: some View {
        var text = "\(inputFileIndex):\(stream.index)"
        text += " \(inputFile.url.lastPathComponent) - \(stream.codecType.text) (\(stream.codecName)"

        // add resolution
        if let width = stream.width, let height = stream.height {
            text += "; \(width)x\(height)"
        }

        // add audio channel layout
        if let channelLayout = stream.channelLayout {
            text += "; \(channelLayout)"
        }

        // check for langauge
        if let lang = stream.language {
            text += "; \(lang)"
        }

        text += ")"
        return Text(text).tag(stream as InputStream?)
    }
}

private struct CodecSettingsButton: View {
    @Binding var outputStream: OutputStream
    @State private var showingSheet = false

    var body: some View {
        Button {
            showingSheet = true
        } label: {
            Text(Image(systemName: "pencil"))
        }.sheet(isPresented: $showingSheet) {
            VideoCodecSettingsView(showingSheet: $showingSheet)
        }
    }
}

private struct VideoCodecSettingsView: View {
    @Binding var showingSheet: Bool

    var body: some View {
        VStack {
            // TODO
            Text("TODO: choose codec and codec settings")
            Button {
                showingSheet = false
            } label: {
                Text("OK")
            }
        }
    }
}

struct StreamMappingView_Previews: PreviewProvider {
    @State static var command = Command.testData

    static var previews: some View {
        StreamMappingView(command: command)
    }
}
