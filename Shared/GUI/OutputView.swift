//
//  OutputView.swift
//  OutputView
//
//  Created by Martin Riedl on 21.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct OutputView: View {
    @ObservedObject var command: Command

    var body: some View {
        SectionView(step: 3, title: "Output") {
            VStack(alignment: .leading) {
                if let fileName = command.output?.lastPathComponent {
                    Text(fileName)
                }
                Button("Output File") {
                    command.updateOutput()
                }
            }
        }
    }
}

struct OutputView_Previews: PreviewProvider {
    @State static var command = Command.testData

    static var previews: some View {
        OutputView(command: command)
    }
}
