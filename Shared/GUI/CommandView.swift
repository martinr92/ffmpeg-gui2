//
//  CommandView.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct CommandView: View {
    @ObservedObject var command: Command
    @State var showRename = false

    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                InputView(command: command)
                if command.inputFiles.count > 0 {
                    StreamMappingView(command: command)
                    OutputView(command: command)
                    EncodeView(command: command)
                }
            }
        }
            .navigationTitle(command.title)
            .toolbar {
            ToolbarItem {
                Button {
                    showRename = true
                } label: {
                    Image(systemName: "pencil")
                }.popover(isPresented: $showRename) {
                    TextField("Name", text: $command.title)
                        .padding()
                }
            }
        }
    }
}

struct CommandView_Previews: PreviewProvider {
    @State static var command = Command.testData

    static var previews: some View {
        CommandView(command: command)
    }
}
