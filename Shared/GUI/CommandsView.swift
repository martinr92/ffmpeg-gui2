//
//  CommandsView.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct CommandsView: View {
    @Binding var mainController: MainController

    var body: some View {
        VStack {
            if mainController.commands.count == 0 {
                Text("There are currntly no FFmpeg commands here. Click on the + button for a new FFmpeg command.")
                    .padding()
            }
            List {
                ForEach($mainController.commands) { $command in
                    NavigationLink(destination: CommandView(command: command)) {
                        Text(command.title)
                    }
                }
            }
        }
            .toolbar {
            ToolbarItem {
                Button {
                    mainController.addCommand()
                } label: {
                    Image(systemName: "plus")
                }
            }
        }
    }
}

struct CommandsView_Previews: PreviewProvider {
    @State static var mainController = MainController.testData

    static var previews: some View {
        CommandsView(mainController: $mainController)
    }
}
