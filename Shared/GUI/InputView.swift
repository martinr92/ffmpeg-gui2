//
//  InputView.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct InputView: View {
    @ObservedObject var command: Command
    @State var lastError: Error?
    @State var showError = false

    var body: some View {
        SectionView(step: 1, title: "Input") {
            VStack(alignment: .leading) {
                ForEach(command.inputFiles.indices, id: \.self) { index in
                    FileView(command: command, index: index, file: command.inputFiles[index])
                }

                if InputOutputSource.filesystem.isAvailable {
                    AddInputButton(command: command, title: "Add File", type: .filesystem)
                }

                if InputOutputSource.photoLibrary.isAvailable {
                    AddInputButton(command: command, title: "Add Photo/Video", type: .photoLibrary)
                }
            }
        }
    }
}

private struct AddInputButton: View {
    @ObservedObject var command: Command
    var title: String
    var type: InputOutputSource
    @State var lastError: Error?
    @State var showError = false

    var body: some View {
        Button(title) {
            Task.init {
                do {
                    try await command.addNewInputFile(source: type)
                } catch {
                    lastError = error
                    showError = true
                }
            }
        }.alert(isPresented: $showError) {
            Alert(title: Text("Error"), message: Text(lastError!.localizedDescription + "\n\(lastError!)"), dismissButton: .default(Text("OK")))
        }
    }
}

private struct FileView: View {
    @ObservedObject var command: Command
    var index: Int
    var file: InputFile

    var body: some View {
        var text = "File \(index): \(file.url.lastPathComponent)"
        if let size = file.size {
            if size > 1024 * 1024 * 1024 {
                // show size in GB
                text += "; Size: \(round(Double(size) / 1024 / 1024 / 1024 * 100) / 100) GB"
            } else {
                // show size in MB
                text += "; Size: \(round(Double(size) / 1024 / 1024 * 100) / 100) MB"
            }
        }
        if let duration = file.duration {
            let minutes = floor(duration / 60)
            let seconds = floor(duration - minutes * 60)
            text += String(format: "; Duration: %02.0f:%02.0f", minutes, seconds)
        }

        return VStack(alignment: .leading) {
            HStack {
                Text(text)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
                if index > 0 {
                    Button {
                        command.moveInputFileUp(index: index)
                    } label: {
                        Image(systemName: "chevron.up")
                    }
                }
                if index + 1 < command.inputFiles.count {
                    Button {
                        command.moveInputFileDown(index: index)
                    } label: {
                        Image(systemName: "chevron.down")
                    }
                }
                Button {
                    command.removeInputFile(index: index)
                } label: {
                    Image(systemName: "trash")
                }
            }
            ForEach(file.streams) { stream in
                StreamView(stream: stream)
            }
        }
    }
}

private struct StreamView: View {
    var stream: InputStream

    var body: some View {
        var text = "Stream \(stream.index): \(stream.codecType.text)"

        // add codec info
        text += "; Codec: \(stream.codecName)"

        // add resolution
        if let width = stream.width, let height = stream.height {
            text += "; Resolution: \(width)x\(height)"
        }

        // add audio channel layout
        if let channelLayout = stream.channelLayout {
            text += "; Channel Layout: \(channelLayout)"
        }

        // check for langauge
        if let lang = stream.language {
            text += "; Language: \(lang)"
        }

        return Text(text)
            .padding(.leading)
    }
}

struct InputView_Previews: PreviewProvider {
    @State static var command = Command.testData

    static var previews: some View {
        Group {
            InputView(command: command)
        }
    }
}
