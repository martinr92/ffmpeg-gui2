//
//  SectionView.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

struct SectionView<Content: View>: View {
    var step: Int
    var title: String
    var content: Content

    init(step: Int, title: String, @ViewBuilder content: () -> Content) {
        self.step = step
        self.title = title
        self.content = content()
    }

    var body: some View {
        GroupBox {
            VStack(alignment: .leading, spacing: 0) {
                HStack {
                    NumberView(number: step)
                    Text(self.title)
                        .font(.title)
                }
                    .padding(EdgeInsets(top: 8, leading: 8, bottom: 4, trailing: 8))
                Divider()
                content
                    .padding(EdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8))
            }
        }
            .padding()
    }
}

private struct NumberView: View {
    var number: Int

    var body: some View {
        Circle()
            .frame(width: 22, height: 22)
            .overlay(
            Text("\(number)")
                .colorInvert()
                .font(.title2)
        )
    }
}

struct SectionView_Previews: PreviewProvider {
    static var previews: some View {
        SectionView(step: 1, title: "Title") {
            Text("OK")
        }
    }
}
