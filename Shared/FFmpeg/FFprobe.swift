//
//  FFprobe.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import ffmpegkit

struct FFprobe {
    static func loadFileInfo(inputFile: String) throws -> FFprobeResponse {
        let arguments = ["-hide_banner", "-v", "quiet", "-print_format", "json", "-show_format", "-show_streams", inputFile]

        // run FFprobe
        let session = FFprobeKit.execute(withArguments: arguments)!

        // check result
        if !session.getReturnCode().isSuccess() {
            throw FFerror.FFProbe
        }

        // parse JSON
        let outputText = session.getOutput()
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let ffprobeResponse = try decoder.decode(FFprobeResponse.self, from: outputText!.data(using: String.Encoding.utf8)!)
            return ffprobeResponse
        } catch {
            print(String(describing: error))
            throw error
        }
    }
}
