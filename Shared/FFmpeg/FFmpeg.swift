//
//  FFmpeg.swift
//  FFmpeg
//
//  Created by Martin Riedl on 26.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import ffmpegkit

class FFmpeg {

    private var session: FFmpegSession?
    private var logText = ""

    func execute(arguments: [String], completed: @escaping(_ success: Bool, _ log: String) -> Void) {
        let command = arguments.map({ argument in
            return "\"\(argument)\""
        }).joined(separator: " ")
        session = FFmpegKit.executeAsync(command) { session in
            if session?.getState() == .completed {
                completed(true, self.logText)
            } else if session?.getState() == .failed {
                completed(false, self.logText)
            }
        } withLogCallback: { log in
            if let text = log?.getMessage() {
                self.logText += text
            }
        } withStatisticsCallback: { statistics in
// TODO
        }
    }

    func cancelExecution() {
        session?.cancel()
    }
}
