//
//  Command.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation

class Command: Identifiable, ObservableObject {
    var id = UUID()
    @Published var title: String
    @Published var inputFiles = [InputFile]()
    @Published var outputStreams = [OutputStream]()
    @Published var output: URL?
    @Published var isEncodingRunning = false
    @Published var encodingCompletedSuccessfully: Bool?
    @Published var encodingLog: String?

    private var currentEncoding: FFmpeg? {
        didSet {
            isEncodingRunning = false
            encodingCompletedSuccessfully = nil
        }
    }

    init(title: String) {
        self.title = title
    }

    private init(title: String, inputFiles: [InputFile], outputStreams: [OutputStream]) {
        self.title = title
        self.inputFiles = inputFiles
        self.outputStreams = outputStreams
    }

    func addNewInputFile(source: InputOutputSource) async throws {
        guard let files = await InputOutput().openFiles(source: .filesystem) else {
            return
        }

        try addNewInputFile(files: files)
    }

    func addNewInputFile(files: [URL]) throws {
        // validate each file
        var newFiles = [InputFile]()
        for file in files {
            let ffprobeResponse = try FFprobe.loadFileInfo(inputFile: file.path)
            let inputFile = InputFile(url: file, ffprobeResponse: ffprobeResponse)
            newFiles.append(inputFile)
        }

        // publish all new files
        for file in newFiles {
            inputFiles.append(file)
        }
    }

    func removeInputFile(index: Int) {
        let removedInputFile = inputFiles.remove(at: index)
        outputStreams.removeAll { outputStream in
            guard let currentInputStream = outputStream.inputStream else {
                return false
            }

            return removedInputFile.streams.contains(where: { $0.id == currentInputStream.id })
        }
    }

    func moveInputFileUp(index: Int) {
        inputFiles.insert(inputFiles.remove(at: index), at: index - 1)
    }

    func moveInputFileDown(index: Int) {
        inputFiles.insert(inputFiles.remove(at: index), at: index + 1)
    }

    func addOutputStream() {
        outputStreams.append(OutputStream())
    }

    func removeOutputStream(index: Int) {
        outputStreams.remove(at: index)
    }

    func updateOutput() {
        // TODO: InputOutput API for iOS
        guard let outputURL = InputOutput().outputFile() else {
            return
        }
        output = outputURL
    }

    func getArguments() -> [String] {
        var args = [String]()

        // add inputs
        for input in inputFiles {
            args += input.getArguments()
        }

        // add streams
        for outputStream in outputStreams {
            args += outputStream.getArguments(command: self)
        }

        // add output
        if output != nil {
            args.append(output!.path)
        }

        return args
    }

    func startExecution() {
        currentEncoding = FFmpeg()
        isEncodingRunning = true
        currentEncoding?.execute(arguments: getArguments()) { success, log in
            DispatchQueue.main.async {
                self.isEncodingRunning = false
                self.encodingCompletedSuccessfully = success
                self.encodingLog = log
            }
        }
    }

    static var testData: Command {
        return Command(title: "Command 1", inputFiles: [InputFile.testData, InputFile.testData, InputFile.testData, InputFile.testData], outputStreams: [OutputStream.testDataVideo])
    }
}
