//
//  InputStream.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation

struct InputStream: Identifiable, Hashable {
    var id = UUID()
    var index: Int
    var codecType: CodecType
    var codecName: String
    var width: Int?
    var height: Int?
    var channelLayout: String?
    var language: String?

    enum CodecType {
        case video, audio, subtitle

        var text: String {
            switch self {
            case .video:
                return "Video"
            case .audio:
                return "Audio"
            case .subtitle:
                return "Subtitle"
            }
        }
    }

    init(ffprobeStream: FFprobeStream) {
        index = ffprobeStream.index
        switch ffprobeStream.codecType {
        case .video:
            codecType = .video
        case .audio:
            codecType = .audio
        case .subtitle:
            codecType = .subtitle
        }
        codecName = ffprobeStream.codecName
        width = ffprobeStream.width
        height = ffprobeStream.height
        channelLayout = ffprobeStream.channelLayout

        // check for language tag
        if let language = ffprobeStream.tags?["language"] {
            self.language = language
        }
    }

    fileprivate init(index: Int, codecType: CodecType, codecName: String, width: Int? = nil, height: Int? = nil, channelLayout: String? = nil, language: String? = nil) {
        self.index = index
        self.codecType = codecType
        self.codecName = codecName
        self.width = width
        self.height = height
        self.channelLayout = channelLayout
        self.language = language
    }

    static var testDataVideo: InputStream {
        return InputStream(index: 0, codecType: .video, codecName: "h264", width: 1920, height: 1080)
    }

    static var testDataAudio: InputStream {
        return InputStream(index: 1, codecType: .audio, codecName: "ac3", channelLayout: "stereo", language: "ger")
    }

    static var testDataSubtitle: InputStream {
        return InputStream(index: 2, codecType: .subtitle, codecName: "dvd_subtitle", language: "eng")
    }
}
