//
//  OutputStream.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 10.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation

struct OutputStream {
    static var unknownID = UUID()

    var inputStream: InputStream?
    var codecSettings: CodecSettings?

    func getArguments(command: Command) -> [String] {
        guard let inputStream = inputStream else {
            return []
        }

        // find input index
        var fileIndex = 0
        for index in 0..<command.inputFiles.count {
            let inputFile = command.inputFiles[index]
            if inputFile.streams.firstIndex(of: inputStream) != nil {
                fileIndex = index
                break
            }
        }

        var args = ["-map", "\(fileIndex):\(inputStream.index)"]

        // add codec specific settings
        if let codecArgs = codecSettings?.getArguments(command: command) {
            args += codecArgs
        }

        return args
    }

    static var testDataVideo: OutputStream {
        return OutputStream(inputStream: InputStream.testDataVideo)
    }
}
