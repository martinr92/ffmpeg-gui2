//
//  InputFile.swift
//  FFmpeg-GUI2
//
//  Created by Martin Riedl on 09.07.21.
//
//    FFmpeg GUI 2
//    Copyright (C) 2021  Martin Riedl
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation

struct InputFile: Identifiable {
    var id = UUID()
    var url: URL
    var size: Int?
    var duration: Double?
    var streams = [InputStream]()

    init(url: URL, ffprobeResponse: FFprobeResponse) {
        self.url = url

        // parse file meta data
        if let size = ffprobeResponse.format.size {
            self.size = Int(size)
        }
        if let duration = ffprobeResponse.format.duration {
            self.duration = Double(duration)
        }

        // parse streams
        if let streams = ffprobeResponse.streams {
            for stream in streams {
                self.streams.append(InputStream(ffprobeStream: stream))
            }
        }
    }

    fileprivate init(url: URL, size: Int?, duration: Double?, streams: [InputStream]) {
        self.url = url
        self.size = size
        self.duration = duration
        self.streams = streams
    }

    func getArguments() -> [String] {
        return ["-i", url.path]
    }

    static var testData: InputFile {
        return InputFile(url: URL(string: "/path/to/file/myFile.mp4")!, size: 2866487, duration: 123.4, streams: [InputStream.testDataVideo, InputStream.testDataAudio])
    }
}
