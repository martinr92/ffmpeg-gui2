#!/bin/sh

#  ci_post_clone.sh
#  FFmpeg-GUI2
#
#  Created by Martin Riedl on 04.11.21.
#  

echo "start execution of ci_post_clone custom script"

# Set the -e flag to stop running the script in case a command returns
# a non-zero exit code.
set -e

# store startup directory for later usage (maybe)
START_DIRECTORY=$(pwd)

# install swiftlint
brew install swiftlint

# download ffmpeg-kit
echo "start processing ffmpeg-kit dependency..."
if [[ $CI_PRODUCT_PLATFORM = "iOS" ]]
then
    curl -o ffmpeg-kit.zip -L https://github.com/tanersener/ffmpeg-kit/releases/download/v4.4/ffmpeg-kit-full-gpl-4.4-ios-xcframework.zip
    FFMPEG_KIT_TARGET_FOLDER=$CI_WORKSPACE/ffmpeg-kit-full-gpl-4.4-ios-xcframework
fi
if [[ $CI_PRODUCT_PLATFORM = "macOS" ]]
then
    curl -o ffmpeg-kit.zip -L https://github.com/tanersener/ffmpeg-kit/releases/download/v4.4/ffmpeg-kit-full-gpl-4.4-macos-xcframework.zip
    FFMPEG_KIT_TARGET_FOLDER=$CI_WORKSPACE/ffmpeg-kit-full-gpl-4.4-macos-xcframework
fi
echo "unzip into folder $FFMPEG_KIT_TARGET_FOLDER"
mkdir $FFMPEG_KIT_TARGET_FOLDER
unzip ffmpeg-kit.zip -d $FFMPEG_KIT_TARGET_FOLDER
echo "ffmpeg-kit dependency successfully processed"

echo "execution of ci_post_clone custom script finished"
